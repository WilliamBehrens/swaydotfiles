#!/bin/sh
#https://github.com/rbnis/dotfiles/blob/master/.config/sway/config

# Variables
set $mod Mod4
set $term alacritty
set $menu wofi --show run --allow-images
set $screenshot grim ~/Pictures/screenshots/scrn-$(date +"%Y-%m-%d-%H-%M-%S").png
set $screenclip slurp | grim -g - ~/Pictures/screenshots/scrn-$(date +"%Y-%m-%d-%H-%M-%S").png
set $background ~/Pictures/Backgrounds/cottage.png


set $tx #ffffff
set $fg #725991
set $bg #000000
set $ex #ef1964

# Colors                border  bg      text    indi     childborder
client.focused          $fg     $bg     $tx     $fg     $fg
client.focused_inactive $bg     $bg     $tx     $bg     $bg
client.unfocused        $bg     $bg     $tx     $bg     $bg
client.urgent           $ex     $ex     $tx     $ex     $ex

# workspaces
set $ws1   1:1
set $ws2   2:2
set $ws3   3:3
set $ws4   4:4
set $ws5   5:5
set $ws6   6:6
set $ws7   7:7
set $ws8   8:8
set $ws9   9:9
set $ws0   10:10
set $wsF1  11:11
set $wsF2  12:12
set $wsF3  13:13
set $wsF4  14:14
set $wsF5  15:15
set $wsF6  16:16
set $wsF7  17:17
set $wsF8  18:18
set $wsF9  19:19
set $wsF10 20:20
set $wsF11 21:21
set $wsF12 22:22

# Font
font pango: GoMono 12

# Window borders
default_border pixel 2
#default_floating_border normal
hide_edge_borders smart

focus_follows_mouse no

smart_gaps on
gaps inner 8

# Autostart
exec --no-startup-id mako
exec pulseaudio --start
exec ~/.local/bin/autotiling
#exec sudo ckb-next-daemon
#exec ckb-next -b

# Input configuration
input * {
    xkb_layout us
    xkb_numlock enable
}

# Output configuration
output * bg $background fill

# Default workspaces for common programs
assign [class="Codium"] $ws5
assign [class="Virt-manager"] $wsF8
#assign [class="Wine"] $wsF12
assign [class="Steam"] $wsF11
assign [app_id="lutris"] $wsF11
assign [class="Ardour"] $ws8


# Start some windows floating
for_window [app_id="dolphin"] floating enable
for_window [app_id="mpv"] floating enable
#for_window [class="Brave"] floating enable
#for_window [class="Steam"] floating enable # have this appear smaller
for_window [class="Wine"] floating enable
for_window [class="QjackCtl"] floating enable # find the window id/class for this
for_window [app_id="lutris"] floating enable
for_window [class="Imv"] floating enable # this still doesn't float for some reason
#for_window [class="Audacious"] floating enable
for_window [app_id="monero-wallet-gui"] floating enable


# Move and resize floating windows with mod+mouse
floating_modifier $mod normal

# Find out how to kill xmrig on unlock
#bindsym $mod+l exec 'swaylock -f --config ~/.config/sway/swaylock_config && xmrig --config ~/Code/Misc/XMRIG_config.json'

# Shortcuts
bindsym $mod+t exec $term
bindsym $mod+Shift+f exec $screenshot
bindsym $mod+Shift+s exec $screenclip
bindsym $mod+d exec $menu

bindsym $mod+Shift+c kill # Kills selected window
bindsym $mod+Shift+q reload
#bindsym $mod+Shift+e exit # exit what
bindsym $mod+h splith
bindsym $mod+v splitv # how do I work these?

#bindsym $mod+l exec $lock
#bindsym $mod+Escape exec $power
#bindsym $mod+End exec $wifi

# Notifications
bindsym Control+Space exec makoctl dismiss
bindsym Control+Shift+Space exec makoctl dismiss --all

# Multimedia
bindsym --locked XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') +5%
bindsym --locked XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') -5%
bindsym --locked XF86AudioMute exec --no-startup-id pactl set-sink-mute $(pacmd list-sinks |awk '/* index:/{print $3}') toggle
bindsym --locked XF86AudioPlay exec playerctl play-pause
bindsym --locked XF86AudioNext exec playerctl next
bindsym --locked XF86AudioPrev exec playerctl previous

# Brightness controls
#bindsym --locked XF86MonBrightnessUp exec --no-startup-id light -A 10
#bindsym --locked XF86MonBrightnessDown exec --no-startup-id light -U 10
# Toggle Redshift
#bindsym $mod+Home exec --no-startup-id pkill -USR1 redshift

#xmrig --config=~/Code/Misc/XMRIG_config.json

# Idle configuration
#exec swayidle \
    #timeout 300 'exec $lock' \
    #timeout 600 'swaymsg "output * dpms off"' \
    #after-resume 'swaymsg "output * dpms on"' \
    #before-sleep 'exec $lock'

# Move your focus around
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# _move_ the focused window with the same, but add Shift
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# switch to workspace
bindsym $mod+1   workspace $ws1
bindsym $mod+2   workspace $ws2
bindsym $mod+3   workspace $ws3
bindsym $mod+4   workspace $ws4
bindsym $mod+5   workspace $ws5
bindsym $mod+6   workspace $ws6
bindsym $mod+7   workspace $ws7
bindsym $mod+8   workspace $ws8
bindsym $mod+9   workspace $ws9
bindsym $mod+0   workspace $ws0
bindsym $mod+F1  workspace $wsF1
bindsym $mod+F2  workspace $wsF2
bindsym $mod+F3  workspace $wsF3
bindsym $mod+F4  workspace $wsF4
bindsym $mod+F5  workspace $wsF5
bindsym $mod+F6  workspace $wsF6
bindsym $mod+F7  workspace $wsF7
bindsym $mod+F8  workspace $wsF8
bindsym $mod+F9  workspace $wsF9
bindsym $mod+F10 workspace $wsF10
bindsym $mod+F11 workspace $wsF11
bindsym $mod+F12 workspace $wsF12

# move focused container to workspace
bindsym $mod+Shift+1    move container to workspace $ws1
bindsym $mod+Shift+2    move container to workspace $ws2
bindsym $mod+Shift+3    move container to workspace $ws3
bindsym $mod+Shift+4    move container to workspace $ws4
bindsym $mod+Shift+5    move container to workspace $ws5
bindsym $mod+Shift+6    move container to workspace $ws6
bindsym $mod+Shift+7    move container to workspace $ws7
bindsym $mod+Shift+8    move container to workspace $ws8
bindsym $mod+Shift+9    move container to workspace $ws9
bindsym $mod+Shift+0    move container to workspace $ws0
bindsym $mod+Shift+F1   move container to workspace $wsF1
bindsym $mod+Shift+F2   move container to workspace $wsF2
bindsym $mod+Shift+F3   move container to workspace $wsF3
bindsym $mod+Shift+F4   move container to workspace $wsF4
bindsym $mod+Shift+F5   move container to workspace $wsF5
bindsym $mod+Shift+F6   move container to workspace $wsF6
bindsym $mod+Shift+F7   move container to workspace $wsF7
bindsym $mod+Shift+F8   move container to workspace $wsF8
bindsym $mod+Shift+F9   move container to workspace $wsF9
bindsym $mod+Shift+F10  move container to workspace $wsF10
bindsym $mod+Shift+F11  move container to workspace $wsF11
bindsym $mod+Shift+F12  move container to workspace $wsF12

# Layout stuff:

# Switch the current container between different layout styles
bindsym $mod+i layout stacking
bindsym $mod+o layout tabbed
bindsym $mod+p layout toggle split
# look for a master stack layout

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+space floating toggle
# Swap focus between the tiling area and the floating area
#bindsym $mod+space focus mode_toggle

# move focus to the parent container
bindsym $mod+a focus parent

# Modes
mode "resize" {
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

#set $mode_system System: (l) lock, (e) logout, (s) suspend, (r) reboot, (S) shutdown, (R) UEFI
#mode "$mode_system" {
#    bindsym l exec $lock, mode "default"
#    bindsym e exit
#    bindsym s exec --no-startup-id systemctl suspend, mode "default"
#    bindsym r exec --no-startup-id systemctl reboot, mode "default"
#    bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
#    bindsym Shift+r exec --no-startup-id systemctl reboot --firmware-setup, mode "default"
#
#    # return to default mode
#    bindsym Return mode "default"
#    bindsym Escape mode "default"
#}
#bindsym $mod+Shift+e mode "$mode_system"

bar {
    position top

    status_command while ~/.config/sway/sway_status; do sleep 1; done

    colors {
        statusline #ffffff
        background #000000

        #focused_workspace #323232AA #323232AA #5c5c5cAA
        #active_workspace
        #inactive_workspace
        #urgent_workspace
   }

    font pango: GoMono 8
}

#include ~/.config/sway/$(hostname)/*
